var mongo = require('mongodb').MongoClient;
var express = require('express');
var auth = require('basic-auth');
var body_parser = require('body-parser')

var busboy = require('connect-busboy');
var fs = require('fs');
var crypto = require('crypto');

 end_points= {'GET':
  {'/ping': pingFunction,
  '/': rootFunction,
  '/exercises': getExList,
   '/exercises/:id': getExID,
 '/exercises/:id/:path': getFileFunction,
	'/grade/:id':get_grade,
	'/readme/:id/:name':get_readme}, 
 'PUT': 
 {'/exercises/:id': addExFunction,
  '/exercises/:id/student': addNewStudFunction,
  '/readme/:id':put_readme,
	'/grade/:id':add_gradefile_handler},
   'DELETE':
    {'/exercises/:id/student/:studentID': deleteStudent,
 '/exercises/:id':removeExercise ,
  '/exercises/:id/file/:file': removeFile,
'/readme/:id/:name':delete_readme}
	}; 

portNum = 8080;

dbAdress = 'mongodb://localhost:27017/exercises';

usr = 'yaron';
passwd = 'MC6I7C7I3P1ADJ';

auth_requ = {'GET': {'/readme/:id/:name':true,'/ping': false, '/': false, '/exercises': false, '/exercises/:id': false, '/exercises/:id/:path': true,'/grade/:id':true}, 
 'PUT': {'/grade/:id':true,'/exercises/:id': true, '/exercises/:id/student': true, '/exercises/:id/file': true,'/readme/:id': true},
  'DELETE': {'/readme/:id/:name':true,'/exercises/:id/student/:studentID': true, '/exercises/:id/file/:file': true,'/exercises/:id':true}};
var app = express(); 
endpointsSetupFunction(app);

var server = app.listen(portNum, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log('Cloudcheck listening at port number: %s', port);
});

 auth_requ = {'GET': {'/ping': false, '/': false, '/exercises': false, '/exercises/:id': false, '/exercises/:id/:path': true}, 
 'PUT': {'/exercises/:id': true, '/exercises/:id/student': true, '/exercises/:id/file': true},
  'DELETE': {'/exercises/:id/student/:student': true, '/exercises/:id/file/:file': true,'/exercises/:id':true}};
function endpointsSetupFunction(app) {
	app.use(body_parser.urlencoded());
	app.use(busboy());

	for (var key in end_points['GET']) {
		if (auth_requ['GET'][key]) {
			app.get(key, authFunction);
		}
    	app.get(key, end_points['GET'][key]);
	}
	for (var key in end_points['PUT']) {
		if (auth_requ['PUT'][key]) {
			app.put(key, authFunction);
		}		
	    app.put(key, end_points['PUT'][key]);
	}
	for (var key in end_points['DELETE']) {
		if (auth_requ['DELETE'][key]) {
			app.delete(key, authFunction);
		}		
	    app.delete(key, end_points['DELETE'][key]);
	}
}

function authFunction(req, res, next) {
	var details = auth(req);

	if (!details || details.name != usr || details.pass != passwd) {
		res.status(403).json('please type the correct username and password!');
		return;
	}

	next();
}

function pingFunction(req, res) {
	res.json({ping: 'pong'});
}

function rootFunction(req, res) {
	var endpoints = {"endpoints": []}
	for (var type in end_points) {
		for (var endpoint in end_points[type]) {
			endpoints["endpoints"].push(type + "   " + endpoint);
		}
	}
	//endpoints=JSON.stringify(endpoints, null, "\t"); // stringify with tabs inserted at each level
	//endpoints=JSON.stringify(endpoints, null, 4); 
	res.send(endpoints);
}

// TODO: Handler completed=true flag as well
function getExList(req, res) {
	var getEx = function(db) { 
		var collection = db.collection('exercises');
		collection.find({}, {'id': 1, 'name': 1, 'version': 1, '_id': 0}).toArray(function(err, docs) {
    		res.json(docs);
		}); 
	}

	mongo.connect(dbAdress, function(err, db) {
		if(err){
			res.status(403).send("Server error");
		}
		getEx(db);
	});	

}

function getExID(req, res) {
	var getExIDNumber = function(db) { 
		console.log (req.params.id);
		var collection = db.collection('exercises');
		collection.find({'id': req.params.id}, {'id': 0, '_id': 0}).toArray(function(err, docs) {
    		console.log (docs);
		
		if(err){ 
		res.json('No such EX');
		}  		
    		else{res.json(docs[0]);}
		}); 
	}

	mongo.connect(dbAdress, function(err, db) {
		if(err){
			res.status(403).send("Server error");
		}
		getExIDNumber(db);
	});	
}

function getFileFunction(req, res) {
	console.log (__dirname + '/files/' + req.params.id + "/" + req.params.path);
	res.download(__dirname + '/files/' + req.params.id + "/" + req.params.path, function(err) {
		if (err) {
			res.status(404).json({"error": 
				{"message": "sorry dude the file doest not exist!",
    			 "code": 404}});
		}
	});
}

function addExFunction(req, res) {
	var insertNewEx = function(db, newEX) { 
		var collection = db.collection('exercises');
		collection.insert(newEX, function(err, result) {
			res.json({"success": {"message": "Exercise added, thank you!", "code": 200}});
			db.close();
		});
	}

	// TODO: Check if req.params.id already exists in the DB. If so, override it
	if (req.body.hasOwnProperty('name')) {
		var newEX = {'id': req.params.id,
					  'name': req.body.name,
					  'version': req.body.version,
					  'comment': req.body.comment,
					  'published_at': new Date().toISOString(),
					  'completed': false,
					  'files': [],
					  'students': [] };

		mongo.connect(dbAdress, function(err, db) {
			insertNewEx(db, newEX);
		});	
	} else {
	console.log(req.body)	
	res.status(400).send("Please add the exercise name in the body! ");
	}
}

// TODO: Handle errors, such as invalid exercise id
function addNewStudFunction(req, res) {
	var addStudent = function(db, ex_id, student) { 
		var collection = db.collection('exercises');
		collection.find({'id': ex_id}).toArray(function(err, docs) {
		    var students = docs[0].students;
		    students.push(student);
		    
    		collection.update({'id': ex_id}, { $set: { 'students': students } }, function(err, result) {
				console.log("the requested student was addedto exercise id %d",ex_id);
				res.json({"success": {"message": "Student was added succesfully!", "code": 200}});
				db.close();
			});
		});      
	}

	if (req.body.hasOwnProperty('name') && req.body.hasOwnProperty('id')) {
		var newStudent = {'id': req.body.id,
					  'name': req.body.name };

		mongo.connect(dbAdress, function(err, db) {
			addStudent(db, req.params.id, newStudent);
		});	
	} else {
		res.status(400).send("Please add id and name fields. please use x-www-form!");
	}

}
function removeExercise(req,res){
	var removeEx = function(db, exercise_id) { 
		var collection = db.collection('exercises');
	
	collection.remove({"id":exercise_id}, function(err, numberOfRemovedDocs) {
	if(err){
	res.json({"failed":{"message":"Exercise does not exist"}});
	return;
		}
		res.json({"success": {"message": "Exercise DELETED succesfully! ", "code": 200}});
	});
}


	mongo.connect(dbAdress, function(err, db) {
		console.log("deleting exercise")
		removeEx(db, req.params.id);
	});	
}


function deleteStudent(req, res) {
	var deleteFunction = function(db, exID, studID) { 
		var collection = db.collection('exercises');
		collection.find({'id': exID}).toArray(function(err, docs) {
		    var students = docs[0].students;
		    var updated_students = students.slice(0);
		    var valid = false
		    for (student_index in students) {
		    	if (students[student_index].id === studID) {
		    		updated_students.splice(updated_students.indexOf(students[student_index]), 1);
		    		valid = true
		    	}
		    }

		    if (!valid) {
		    	console.log("the stud does not exist")
		    	res.status(400).json({"error": {"message": "wrong id", "code": 400}});
		    	return
		    }
		    
    		collection.update({'id': exID}, { $set: { 'students': updated_students } }, function(err, result) {
				console.log("removed exercise student");
				res.status(200).json({"success": {"message" : "removed student from exercise", "code" : 200}});
				db.close();
			});
		});      
	}

	mongo.connect(dbAdress, function(err, db) {
		deleteFunction(db, req.params.id, req.params.studentID);
	});	
}

function removeFile(req, res) { 
	var deleteEx = function(db, exercise_id, filename) { 
		var collection = db.collection('exercises');
		collection.find({'id': exercise_id}).toArray(function(err, docs) {
		    assert.equal(err, null);
		    assert.equal(1, docs.length);
		    console.log("Found the following records");
		    console.dir(docs);

		    var files = docs[0].files;
		    var updated_files = files.slice(0);
		    var file_removed = false
		    for (id in files) {
		    	if (files[id].path === filename) {
		    		console.log("File is removed");
		    		updated_files.splice(updated_files.indexOf(files[id]), 1);
		    		file_removed = true
		    	}
		    }

		    if (!file_removed) {
		    	console.log("file was not found")
		    	res.status(400).json({"error": {"message": "No such file exists", "code": 400}});
		    	return
		    }
		    
    		collection.update({'id': exercise_id}, { $set: { 'files': updated_files } }, function(err, result) {
				console.log("removed exercise file");
                res.json({"success": {"message": "File removed succesfully", "code": 200}});
				db.close();
				fs.unlinkSync(__dirname + '/files/' + exercise_id + "/" + filename)

			});
		});      
	}

	

	mongo.connect(dbAdress, function(err, db) {
		deleteEx(db, req.params.id, req.params.fileName);
	});
}



function put_readme(req,res){

        if (req.busboy) {
        req.busboy.on('file', function(fieldname, file, filename) {
                if (fieldname != "manifest") {
                        res.status(400).json({"error": {"message": "Missing 'gradefile' field", "code": 400}});
                        return;
                }

            console.log("Uploading: " + filename);

            var full_filename = __dirname + '/readme/' + req.params.id + "/" + filename
            console.log (full_filename);
            var fstream = fs.createWriteStream(full_filename);
            file.pipe(fstream);
            fstream.on('close', function () {
                console.log("Upload Finished of " + filename);

                                var fd = fs.createReadStream(full_filename);
                                var hash = crypto.createHash('sha1');
                                hash.setEncoding('hex');

                                fd.on('end', function() {
                                    hash.end();
                                    sha1_hash = hash.read();
                                    console.log("readme hash is " + sha1_hash);

                                    filesize = fs.statSync(full_filename)["size"];
                                    console.log("readme size is " + filesize);
                                });

                                fd.pipe(hash);
                                res.json({"success": {"message": "File uploaded succesfully", "code": 200}});
            });
             fstream.on('error', function(err) {
                console.log("readme " + filename + " failed uploading");
                res.status(400).json({"error": {"message": "readme upload failed", "code": 400}});
                });
                });

        req.pipe(req.busboy);
        } else {
                res.status(400).json({"error": {"message": "Missing readme data", "code": 400}});
        }
}




function get_readme(req,res){
console.log(req.params)
console.log(__dirname + '/readme/' + req.params.id + '/'  + req.params.name)
res.download(__dirname + '/readme/' + req.params.id + '/'  + req.params.name, req.params.file, function(err) {
                if (err) {
                        res.status(404).json({"error":
                                {"message": "exercise '" + req.params.id + "' does not have a '" + req.params.file + "' readme'",
                         "code": 404}});
                }
        });


}
function get_exercise_file_handler(req, res) {
        console.log (__dirname + '/files/' + req.params.id + "_" + req.params.path);
        res.download(__dirname + '/files/' + req.params.path, req.params.path, function(err) {
                if (err) {
                        res.status(404).json({"error":
                                {"message": "exercise '" + req.params.id + "' does not have a '" + req.params.path + "' file.",
                         "code": 404}});
                }
        });
}



function delete_readme(req,res){
        fs.unlink( __dirname + '/readme/' + req.params.id + "/" + req.params.name , function (err) {
                if (err) {
                        console.log ("the file dont exist");
                         res.status(400).json({"error": {"message": "Missing  readme file", "code": 400}});
                        return;
                }
                console.log('successfully deleted /tmp/hello');
                res.json({"success": {"message": "readme file deleted OK", "code": 204}});
        });
}

function add_gradefile_handler(req,res){
        var update_document = function(db, exercise_id, grade_record) {
                var collection = db.collection('exercises');
                collection.find({'id': exercise_id}).toArray(function(err, docs) {
         
        
                collection.update({'id': exercise_id}, { $set: { 'grade': grade_record.grade_int } }, function(err, result) {
      
                                console.log("update exercise grade");
                res.json({"success": {"message": "yourgrade.txt uploaded OK", "code": 200}});
                                db.close();
                        });
                });
        }

        if (req.busboy) {
        req.busboy.on('file', function(fieldname, file, filename) {
                if (fieldname != "gradefile") {
                        res.status(400).json({"error": {"message": "Missing 'gradefile' field", "code": 400}});
                        return;
                }

            console.log("Uploading: " + filename);

            var full_filename = __dirname + '/files/grades/' + req.params.id + "/" + filename
            console.log (full_filename);
            var fstream = fs.createWriteStream(full_filename);
            file.pipe(fstream);
            fstream.on('close', function () {
                console.log("Upload Finished of " + filename);

                                var fd = fs.createReadStream(full_filename);
                                var hash = crypto.createHash('sha1');
                                hash.setEncoding('hex');

                                fd.on('end', function() {
                                    hash.end();
                                    sha1_hash = hash.read();

                                    filesize = fs.statSync(full_filename)["size"];
                                    grade_int = fs.readFileSync(full_filename).toString().split("\n")[0];
                                 
                                    grade_record = {"path": filename, "sha1": sha1_hash, "size": filesize, "grade_int" : grade_int  };

                                        mongo.connect(dbAdress, function(err, db) {
     
                                                update_document(db, req.params.id, grade_record);
                                        });
                                });

                                fd.pipe(hash);
            });
            fstream.on('error', function(err) {
                console.log("gradefile " + filename + " failed uploading");
                res.status(400).json({"error": {"message": "gradefile upload failed", "code": 400}});
                });
     		});

        req.pipe(req.busboy);
        } else {
                res.status(400).json({"error": {"message": "Missing gadefile data", "code": 400}});
        }


}


function get_grade(req,res){

        var get_grade_by_id = function(db) {
                console.log (req.params.id);
                var collection = db.collection('exercises');
                collection.find({'id': req.params.id}, { '_id': 0, 'students': 1, 'name': 1, 'grade': 1 }).toArray(function(err, docs) {
                console.log (docs);
          
        
                res.json(docs[0]);
                });
        }

        mongo.connect(dbAdress, function(err, db) {
          
                get_grade_by_id(db);
        });

}
